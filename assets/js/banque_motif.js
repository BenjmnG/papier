for (var yi = 0; forms.y < clientHeight + forms.h*2; yi++) {
	for (var xi = 0; forms.x < clientWidth + forms.w*4; xi++) {
		var interval = 7.5;
		for (var e = - 30; 0 < forms.h - e;) {
			//Grue / bottom straight
			sketch.stroke("#f74820");
			sketch.strokeWeight(2);
			sketch.beginShape();
			  sketch.vertex(forms.x - forms.w/2, forms.y - forms.h + e);
			  sketch.vertex(forms.x + forms.w/2 + e - 20, forms.y);
			  sketch.vertex(forms.x - forms.w/2, forms.y + forms.h );
			  sketch.vertex(forms.x - forms.w*1.5 - e + 20, forms.y);
			  sketch.vertex(forms.x - forms.w/2, forms.y - forms.h + e);
			sketch.endShape();
			e = e + interval;
		}
    }
}

for (var yi = 0; forms.y < clientHeight + forms.h*2; yi++) {
	for (var xi = 0; forms.x < clientWidth + forms.w*4; xi++) {
		var interval = 7.5;
		for (var e = - 30; 0 < forms.h - e;) {
			// Damier : V et V
			sketch.beginShape();
				sketch.vertex(forms.x + forms.w/2 + e, forms.y - forms.h - f - 40);   
					sketch.vertex(forms.x - forms.w/2, forms.y + forms.h + e);
				sketch.vertex(forms.x - forms.w*1.5 - e, forms.y - forms.h - f - 40);
				sketch.endShape();
				sketch.beginShape();
				sketch.vertex(forms.x + forms.w/2 + e, forms.y + forms.h + f + 40);   
					sketch.vertex(forms.x - forms.w/2, forms.y - forms.h - e);
				sketch.vertex(forms.x - forms.w*1.5 - e, forms.y + forms.h + f + 40);
			sketch.endShape();
			e = e + interval;
		}
    }
}

for (var yi = 0; forms.y < clientHeight + forms.h*2; yi++) {
	for (var xi = 0; forms.x < clientWidth + forms.w*4; xi++) {
		var interval = 7.5;
		for (var e = - 30; 0 < forms.h - e;) {
	          // Pattern : Losange
	          sketch.stroke("#f74820");
	          //sketch.fill('#fff');
	          sketch.strokeWeight(2);
	          sketch.beginShape();
	            sketch.vertex(forms.x - forms.w/2, forms.y - forms.h + e);
	            sketch.vertex(forms.x + forms.w/2 + 40, forms.y);
	            sketch.vertex(forms.x - forms.w/2, forms.y + forms.h - e);
	            sketch.vertex(forms.x - forms.w*1.5 - 40, forms.y);
	            sketch.vertex(forms.x - forms.w/2, forms.y - forms.h + e);
	          sketch.endShape();
			e = e + interval;
		}
    }
}

for (var yi = 0; forms.y < clientHeight + forms.h*2; yi++) {
	for (var xi = 0; forms.x < clientWidth + forms.w*4; xi++) {
		var interval = 7.5;
		for (var e = - 30; 0 < forms.h - e;) {
			// Losange : Straight
			sketch.stroke("#454545");
			sketch.fill('#f2f542');
			sketch.strokeWeight(2);
			sketch.beginShape();
				sketch.vertex(forms.x - forms.w/2, forms.y - forms.h + e);
				sketch.vertex(forms.x + forms.w/2 + 0, forms.y);
				sketch.vertex(forms.x - forms.w/2, forms.y + forms.h - e);
				sketch.vertex(forms.x - forms.w*1.5 - 0, forms.y);
				sketch.vertex(forms.x - forms.w/2, forms.y - forms.h + e);
			sketch.endShape();

			e = e + interval;
		}
    }
}


for (var yi = 0; forms.y < clientHeight + forms.h*2; yi++) {
	for (var xi = 0; forms.x < clientWidth + forms.w*4; xi++) {
		var interval = 7.5;
		for (var e = - 30; 0 < forms.h - e;) {
			// Losange : Incurvation intérieure ( Carreau)
			sketch.stroke("#454545");
			sketch.fill('#f2f542');
			sketch.strokeWeight(2);
			sketch.beginShape();
	            sketch.vertex(forms.x - forms.w/2, forms.y - forms.h + e);
	            sketch.vertex(forms.x + forms.w/2 + e + 4, forms.y);
	            sketch.vertex(forms.x - forms.w/2, forms.y + forms.h - e);
	            sketch.vertex(forms.x - forms.w*1.5 - e - 4, forms.y); //(forms.x - forms.w*2, forms.y)
	            sketch.vertex(forms.x - forms.w/2, forms.y - forms.h + e);
			sketch.endShape();

			e = e + interval;
		}
    }
}


