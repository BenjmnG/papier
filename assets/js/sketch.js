/////

const wavecolor = ( sketch ) => {

  sketch.setup = () => {

    var clientHeight = document.getElementById('page_1').clientHeight;
    var clientWidth = document.getElementById('page_1').clientWidth;
    
    sketch.createCanvas(clientWidth, clientHeight, sketch.SVG)
    sketch.fill('black');

    //global rules
    sketch.rectMode(sketch.CENTER);
    sketch.noStroke();
    sketch.rectMode(sketch.CENTER);

  // Draw all the form
    forms = {
      h : clientHeight/60,
      w : clientWidth/15,
      x : 0,
      y : 0,
      stroke : 8,
    }

    var coord = [];
    var weight = [];

    //#5abd7e #c9e6cf #8ab4ea #fdb944 #2b798f
    
    var xoff = 0;
    var xweight = 0;
    var yweight = 0;
    var xdecalage = 0;

    for (var xi = 0; forms.x < clientWidth + forms.w; xi++) {
      
      var firstnoise = sketch.noise(xoff) * forms.w * sketch.noise(xoff) ;
      var firstweight = sketch.noise(xweight) * forms.stroke ;
     
     // reset offset, now based on x
      var yoff = xoff;     

      for (var yi = 0; forms.y < clientHeight + forms.h; yi++) {

        var secondnoise = sketch.noise(yoff) * forms.w * sketch.noise(yoff);
        var secondweight = sketch.noise(yweight) * forms.stroke;

        if(yi === 0){ 
          // first y is based on first x
          noise = firstnoise; 
          stroke  = firstweight;
        } else { 
          noise = secondnoise;
          stroke = secondweight
        }

        decalage = noise/10

        // draw Pattern 1
        sketch.strokeWeight(stroke);
        sketch.fill('#FB0A3D');
        //sketch.noFill();   
       /* sketch.beginShape();
          sketch.vertex(forms.x, forms.y);
          sketch.vertex(forms.x + noise, forms.y + forms.h); 
          sketch.vertex(forms.x, forms.y + forms.h); 
          sketch.vertex(forms.x + noise, forms.y);
        sketch.endShape();*/
        sketch.circle(forms.x, forms.y, noise);
        sketch.fill('#0050ff');
        sketch.circle(forms.x + decalage, forms.y + decalage, noise) 

        // clarify x & y
        forms.y = forms.y + forms.h;

        //progress in y line
        yoff += .25;
        yweight += .25;

        // populate an buffer-array & manage it
        if(xi === 0){
          coord.push(noise);
          weight.push(weight);
        } else {
          coord.splice(yi, 1, noise);
          weight.splice(yi, 1, weight);
        }

      }
      // clarify x & y
      forms.x = forms.x + firstnoise;
      forms.y = 0;



      //progress in x line
      xoff += .5;
      xweight += .45;
    }
  }
}

let pwavecolor = new p5(wavecolor, 'page_1');

/////
