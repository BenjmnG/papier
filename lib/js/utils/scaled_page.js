function fillDiv(div, proportional) {
  //var currentWidth = div.outerWidth();
  //var currentHeight = div.outerHeight();

  var page = document.getElementById('pages');

  var clientWidth = document.getElementById('pages').clientWidth;
  var clientHeight = document.getElementById('pages').clientHeight;

  var availableHeight = window.innerHeight;
  var availableWidth = window.innerWidth;

  var scaleX = availableWidth / clientWidth;
  var scaleY = availableHeight / clientHeight;

  if (proportional) {
    scaleX = Math.min(scaleX, scaleY);
    scaleY = scaleX;
  }

  //var translationX = Math.round((availableWidth - (clientWidth * scaleX)) / 2);
  //var translationY = Math.round((availableHeight - (clientHeight * scaleY)) / 2);

  var scale = Math.min(scaleX, scaleY);

  page.style.transform = 'scale(' + scale*.9 + ')';
}

fillDiv();
